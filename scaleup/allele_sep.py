import numpy as np
import pandas as pd
import os
import os.path as osp
import re
import tables
import sklearn as skl
import sklearn.neighbors
import sklearn.cluster
import scipy
import scipy.spatial.distance as sp
import scipy.stats as st
import matplotlib.pyplot as plt
import tqdm
import bokeh
import bokeh.io
import bokeh.plotting
import plotly.express as px



'''
allele-sep.py: functions and classes for performing genome-aware allele separation 
on DNA FISH coordinates.
Lincoln Ombelets 2020
'''



def discrete_dist(a, b):
    
    return int(a != b)


def discrete_sim(a, b):
    
    return int(a == b)


def is_neighbor(a, b):
    
    return int(abs(a-b) == 1)


def inflate_selfdist(chrom, data):
    
    
    regions = np.atleast_2d(data['Region ID'].values).T
    pts = data[['xnm', 'ynm', 'znm']]
    
    self_indicator = sp.pdist(regions, metric=discrete_sim)
    dists = sp.pdist(pts, metric='euclidean')
    
    return sp.squareform(1e9*self_indicator + dists)


def compute_n(chrom, data):
    
    
    return 1 + int(np.ceil(data['Region ID'].value_counts().mean()))


def nonself_discrete(chrom, data):
    
    
    regions = np.atleast_2d(data['Region ID'].values).T
    
    return sp.squareform(sp.pdist(regions, metric=discrete_dist))


def neighbor_discrete(chrom, data):
    
    
    regions = np.atleast_2d(data['Region ID'].values).T
    
    return sp.squareform(sp.pdist(regions, metric=is_neighbor))


def make_neighbor_matrix(chrom, data):
    
    
    data = data.sort_values(by='Region ID')
    
    data['Region rank'] = data['Region ID'].rank(method='dense')
    
    ranks = np.atleast_2d(data['Region rank'].values).T
    
    return sp.squareform(sp.pdist(ranks, metric=is_neighbor))


def scaling_inflation(chrom, data):
    
    
    regions = np.atleast_2d(data['Region ID'].values).T
    pts = data[['xnm', 'ynm', 'znm']]
    
    self_indicator = sp.pdist(regions, metric=discrete_sim)
    
    genomic_pdists = sp.pdist(regions, metric='euclidean') # it's 1D so same as cityblock
    
    spatial_dists = sp.pdist(pts, metric='euclidean')
    
    return sp.squareform(1e9*self_indicator + genomic_pdists * spatial_dists)
    

def path_length(dist, path, metric='euclidean', return_lens=False):
    '''
    Compute the distance traversed in the space (graph) defined by `dist` 
    on the path whose vertices are defined by `path`.
    `dist` can be a precomputed distance matrix (or a lower triangular vector)
    or a list of points (if it is 2D but not square).
    
    Returns the sum of the pairwise distances specified by `path`, as well
    as the individual distances if `return_lens`=True.
    '''
    
    if len(path) < 2:
        if return_lens:
            return [0], 0
        else:
            return 0
    
    
    if len(dist.shape) == 1:
        # we have a lower-triangular distance matrix
        dist = sp.squareform(dist)
    elif dist.shape[0] > dist.shape[1]:
        # we have a list of points
        dist = sp.squareform(sp.pdist(dist, metric=metric))
    
    # form pairwise edges from the adjacent points in path
    edges = np.array([ 
        [ path[i], path[i+1] ] for i in range(len(path)-1) 
    ])
    
    # retrieve the distance along each edge (note tuple needed for indexing)
    edgelens = [ dist[tuple(e)] for e in edges ]
    
    totallen = np.sum(edgelens)
    
    if return_lens:
        return edgelens, totallen
    
    # return sum of distances
    return totallen
    
def summarize_clusters(df, label_col='Label', locus_col='Region ID'):
    '''
    Take a labeled dataframe and return a summary dataframe describing some
    allele separation metrics.
    1. Total loci in each cluster
    2. Percent of loci in each cluster that are nonunique (occur >1 time)
    3. Total path length traversed along genomic order within cluster
    4. Standard deviation of individual path components in the above traversal
    '''
    
    
    
    labels = []
    cluster_totals = []
    percent_multis = []
    pathlens = []
    pathstds = []
    pathmaxes = []
    
    for label, group in df.groupby(label_col):
        
        group = group.sort_values(by=locus_col)
        
        # Find the length traversed through the whole cluster in the order of genomic coord
        # Note this computes pairwise distances again after clustering. Can store these instead.
        pathcomps, pathlen = path_length(
            group[['xnm', 'ynm', 'znm']].values,
            np.arange(0, len(group), 1),
            return_lens=True
        )
        
        pathstd = np.std(pathcomps)
        pathmax = np.max(pathcomps)
        
        # Count how many times each locus appears, then how many times each of these counts appears
        # I.e. find out how many loci only appear once, how many appear twice, three times...
        # which we call multiplicity
        loci_mult = pd.DataFrame(group[locus_col].value_counts().value_counts()).reset_index()
        
        loci_mult.rename(columns={locus_col: 'Num loci',
                                  'index': 'Multiplicity',
                                 },
                        inplace=True)
                
        # Total loci in the cluster
        cluster_total = group['Region ID'].nunique()
        # How many loci are not unique i.e. appear more than once in the cluster
        percent_multi = loci_mult.query('Multiplicity > 1')['Num loci'].sum() / cluster_total
        
        labels.append(label)
        pathlens.append(pathlen)
        pathstds.append(pathstd)
        pathmaxes.append(pathmax)
        cluster_totals.append(cluster_total)
        percent_multis.append(percent_multi)
            
    
    sil = skl.metrics.silhouette_score(df[['xnm', 'ynm', 'znm']].values,
                                 df[label_col].values, 
                                 metric='euclidean')
    
    chs = skl.metrics.calinski_harabasz_score(df[['xnm', 'ynm', 'znm']].values,
                                 df[label_col].values, 
                                 )
    
    dbs = skl.metrics.davies_bouldin_score(df[['xnm', 'ynm', 'znm']].values,
                                 df[label_col].values, 
                                 )
    
    summary = pd.DataFrame({'Label': labels,
                            'Total loci': cluster_totals,
                            'Percent nonunique loci': percent_multis,
                            'Path length': pathlens,
                            'Path std dev': pathstds,
                            'Maximum jump': pathmaxes,
                            'Silhouette': sil,
                            'Calinski-harabasz': chs,
                            'Davies-bouldin': dbs
                           })
    
    return summary


def agg_alleles(
    df,
    points=None,
    n_clusters=3,
    affinity='euclidean',
    linkage='ward',
    connectivity=None,
    distance_threshold=None,
    labels=['green', 'red', 'blue', 'orange', 'grey', 'magenta'],
    return_df=False
):
   

    if distance_threshold is not None:
        n_clusters=None
    
    agg = skl.cluster.AgglomerativeClustering(
        n_clusters=n_clusters,
        affinity=affinity,
        connectivity=connectivity,
        linkage=linkage,
        distance_threshold=distance_threshold
    )
    
    if points is None:
        points = df[['xnm', 'ynm', 'znm']].values
    
    predict = agg.fit_predict(points)
    
    df['Label'] = predict
            
    
    summary = summarize_clusters(df)

    if return_df:
        return df, summary
    
    return summary



def agg_cell(
    cell_df,
    points=None,
    affinity='euclidean',
    linkage=None,
    n_clusters=None,
    connectivity=None,
    distance_threshold=None,
    save_results=False,
):    
    
    
    ### Test if we have a callable function to supply points or connectivity matrix.
    
    try:
        points.__iter__
        ## If passed an array-like, return the specified columns
        _points = list(points)
        points = lambda c, d: d[_points].values
    except:
        pass
    
    if points is None:
        ## Default to a dummy function that just returns the proper columns
        _points = ['xnm', 'ynm', 'znm']
        points = lambda c, d: d[['xnm', 'ynm', 'znm']].values
        
        
    ### Points callable receives the chromosome dataframe and returns either a list of points or a square distance matrix
    ## Note that if it returns a square distance matrix, `affinity` must be precomputed and `linkage` cannot be Ward (which is the default)
    try:
        points.__call__
    except AttributeError:
        raise TypeError('agg_cell: `points` argument must be None, a list of columns, or a function on the chromosome ID and its dataframe.')
    
    
    connectivity_callable = True
    n_clusters_callable = True
    
    ### Connectivity callable receives the chromosome dataframe and returns a square connectivity matrix
    try:
        connectivity.__call__
    except AttributeError:
        connectivity_callable = False
        
    ### n_clusters callable receives chromosome dataframe and returns an integer    
    try:
        n_clusters.__call__
    except AttributeError:
        _n_clusters = n_clusters
        n_clusters = lambda c, d: _n_clusters
    
    results = []
    summaries = []
    
    if distance_threshold is not None:
        n_clusters = None
    
    ##### Loop through each chromosome
    for chrom, group in cell_df.groupby('Chrom ID'):
        
        ## Ensure we are sorted by increasing region ID
        group = group.sort_values(by='Region ID')
        
        ## Call our points function
        chrom_points = points(chrom, group)
        
        if distance_threshold is None:
            chrom_clusters = int(n_clusters(chrom, group))
        else:
            chrom_clusters = None
        
        if connectivity_callable:
            chrom_connectivity = connectivity(chrom, group)
        else:
            chrom_connectivity=None
            
        chrom_agg = agg_alleles(
            df=group,
            points=chrom_points,
            n_clusters=chrom_clusters,
            affinity=affinity,
            connectivity=chrom_connectivity,
            linkage=linkage,
            distance_threshold=distance_threshold,
            return_df=save_results
        )
        
        if save_results:
            results.append(chrom_agg[0])
            chrom_agg[1]['Chrom ID'] = chrom
            summaries.append(chrom_agg[1])
        else:
            chrom_agg['Chrom ID'] = chrom
            summaries.append(chrom_agg)
        
    summaries_concat = pd.concat(summaries, ignore_index=True)

    if save_results:
        results_concat = pd.concat(results, ignore_index=True)
        
        return results_concat, summaries_concat
    
    return summaries_concat




###############################################################################
# "ad hoc" approach
###############################################################################


def make_matrices(df, 
                  spatial_thresh=None, 
                  genomic_thresh=None,
                  spatial_mode='distance',
                  genomic_mode='distance',
                 ):
    
    
    df = df.sort_values(by='Region ID')
    
    spatial_pts = df[['xnm', 'ynm', 'znm']].values
    
    genomic_pts = df['Region ID'].rank(method='dense').values
    genomic_pts = np.atleast_2d(genomic_pts).T
    
    if spatial_thresh is not None:
        
        spatial_matrix = skl.neighbors.radius_neighbors_graph(
            spatial_pts,
            spatial_thresh, 
            mode='distance'
        ).toarray()
    
    else:
        spatial_matrix = sp.squareform(sp.pdist(spatial_pts))
    
    if genomic_thresh is not None:
        
        genomic_matrix = skl.neighbors.radius_neighbors_graph(
            genomic_pts,
            genomic_thresh,
            mode='distance'
        ).toarray()
        
    else:
        genomic_matrix = sp.squareform(sp.pdist(genomic_pts))
    
    
    if spatial_mode.lower() == 'connectivity':
        spatial_matrix = spatial_matrix > 0
    
    if genomic_mode.lower() == 'connectivity':
        genomic_matrix = genomic_matrix > 0
        
    return [ 
        spatial_matrix, 
        genomic_matrix,
        genomic_pts.ravel()
    ]


def remove_node(matrices, nodes):
    
    
    result = [ m.copy() for m in matrices ]
        
    for node in np.atleast_1d(nodes):
        for matrix in result[:2]:
            matrix[:, node] = 0#np.ma.masked
            matrix[node, :] = 0#np.ma.masked
    
    return result
        
        

def get_valid_connections(matrices, 
                          node,
                          removed=[],
                          path=None,
                          master=None,
                          path_max_dist=5000,
                          spatial_thresh=None, 
                          genomic_thresh=None,
                          allow_back=True,
                          costfunc=None,
                        ):
    
    
    if node in removed:
        return [ [], [] ]
    
    if spatial_thresh is None:
        spatial_thresh = 5000
    if genomic_thresh is None:
        genomic_thresh = 20
        
    if costfunc is None:
        costfunc = np.multiply
        
    cost_thresh = costfunc(np.atleast_1d(spatial_thresh), np.atleast_1d(genomic_thresh))
    
    spatial_row = matrices[0][node]
    genomic_row = matrices[1][node]
    
    spatial_filter = np.bitwise_and(spatial_row <= spatial_thresh, spatial_row > 0)
    genomic_filter = genomic_row > 0
    
    #indiv_filter = np.bitwise_and(spatial_filter, genomic_filter)
    
    costs = costfunc(spatial_row, genomic_row)
    #
    #cost_filter = costs <= cost_thresh
    #
    #combined_filter = np.bitwise_and(indiv_filter, cost_filter)
    #combined_filter = indiv_filter
    
    combined_filter = np.bitwise_and(spatial_filter, genomic_filter)
    
    if not allow_back:
        combined_filter[:node] = False
    
    dest_indices = np.flatnonzero(combined_filter)
    filtered_costs = np.compress(combined_filter, costs)

    # We have to compare to each member of the current cluster
    if path is not None and master is not None:
        # make all pairwise combinations of possible 
        # destination indices with the indices in the current cluster
        #path_dest_pairs = list(product(dest_indices, path))
        #
        # look up the distances from the master pairwise distance matrix
        #path_dest_dists = np.array([
        #    master[0][pair]
        #    for pair in path_dest_pairs
        #]).reshape((len(dest_indices), len(path)))
        #
        #path_dest_gens = np.array([
        #    master[1][pair]
        #    for pair in path_dest_pairs
        #]).reshape((len(dest_indices), len(path)))
        #
        #path_dest_costs = costfunc(path_dest_dists, path_dest_gens)
        #
        # find the row indices where all members of the current path
        # are at most 8000 nm away
        #pdist_thresh = np.all(path_dest_dists < path_max_dist, axis=1)

        #good_rows = np.flatnonzero(
        #    pdist_thresh
        #)
        
        # restrict the dest indices to those that passed
        # this condition
        #dest_indices = dest_indices[good_rows]
        #filtered_costs = filtered_costs[good_rows]
        
        good_rows = []
        order_path, order_total = path_length(master[0], sorted(path), return_lens=True)
        
        for i, cand in enumerate(dest_indices):
            cand_path = sorted(path + [cand])
            
            cand_lens, _ = path_length(master[0], cand_path, return_lens=True)
            
            if np.all(np.array(cand_lens) < path_max_dist):
                good_rows.append(i)
        
        dest_indices = dest_indices[good_rows]
        filtered_costs = filtered_costs[good_rows]
    
    cost_sorter = np.argsort(filtered_costs)
    
    return [ dest_indices[cost_sorter], filtered_costs[cost_sorter] ]


def find_seeds(
    matrices, 
    spatial_thresh=10000, 
    genomic_thresh=10, 
    approx_n=3,
    costfunc=None
):
    
    
    def seed_moves(seeds, spatial_thresh, genomic_thresh):
        
        
        seed_cons = [ 
            get_valid_connections(
                matrices, 
                seed, 
                spatial_thresh=spatial_thresh,
                genomic_thresh=genomic_thresh,
                allow_back=False,
                costfunc=costfunc
            ) for seed in seeds ]
        
        
        unique_cons = []
        unique_costs = []
        
        # for each set of valid moves for a seed:
        for i, con in enumerate(seed_cons):
            
            # con_inds holds the valid indices
            con_inds = con[0]
            con_costs = con[1]
            
            if len(con_inds) == 0:
                unique_cons.append([])
                unique_costs.append([])
                continue
            
            # take the union of the dests reachable from the **other** seeds
            # if/else needed for final con, since con_inds[i+1] would throw an error
            other_cons = set()
            
            if i < len(seed_cons):# - 1:
                other_cons = set.union(*(set(s[0]) for s in seed_cons[0:i]+seed_cons[i+1:]))
                #other_cons = {other_cons | set(s[0]) for s in seed_cons[i+1:]}
                #print(other_cons)
            else:
                print(i)
                other_cons = set.union(*(set(s[0]) for s in seed_cons[0:i-1]))
            
            # find the **unique** members of the current seed's dests by set difference
            unique = np.array(list(set(con_inds) - other_cons - set(seeds)))
            #print(set(con_inds)-set(seeds))
            
            # find the indices of these unique dests in con_inds
            unique_indices = np.flatnonzero(
                np.isin(con_inds, unique)
            )
            
            unique_cons.append(con_inds[unique_indices])
            unique_costs.append(con_costs[unique_indices])
            
        return [unique_cons, unique_costs]
    
    seeds = np.arange(0, approx_n, 1)
    
    for _ in range(100):
        
        seed_scores = seed_moves(
            seeds, 
            spatial_thresh=spatial_thresh, 
            genomic_thresh=genomic_thresh
        )
        
        # find which seed(s) have no unique moves
        nomoves = np.flatnonzero([ len(c) == 0 for c in seed_scores[0] ])
        #badcost = np.flatnonzero([ np.all(np.array(c) > move_thresh) for c in seed_scores[1] ])
        
        badseeds = list(set(nomoves))# | set(badcost))

        if len(badseeds) > 0:
            #print(seeds, badseeds)
            for i, s in zip(badseeds, seeds[badseeds]):
                seeds[i] = s+1
        else:
            break
    
    return seeds, seed_scores


def remove_locus(mats, node=None, loc=None):


    if node is not None:
        loc = mats[2][node]
    
    other_copies = np.flatnonzero(mats[2]==loc)
    
    return remove_node(mats, other_copies)


def next_seed(mats, 
              all_dots, 
              removed, 
              sp_thresh, 
              gen_thresh,
              n_tries=5
             ):
    
    
    cands = sorted(list(set(all_dots) - set(removed)))
    found_cand = False

    n = 0
    
    for cand in cands:
            
        for _ in range(n_tries):
            
            nextmoves = get_valid_connections(
                mats, 
                cand,
                allow_back=False,
                spatial_thresh=sp_thresh,
                genomic_thresh=gen_thresh,
            )

            if len(nextmoves[0]) == 0:
                sp_thresh += 300
                gen_thresh += 2
            else:
                found_cand = True
                break
        if found_cand:
            return cand
    
    # if we get here, we didn't find *any* valid candidate in the remaining nodes
    
    return None

def adhoc_alleles(
    data,
    n_seeds=3,
    sp_thresh_max=5000,
    gen_thresh_max=100,
):
    

    # preserve the original distance matrix
    mats_master = make_matrices(data)
    # make a working copy
    mats = [ m.copy() for m in mats_master ]


    all_dots = np.arange(0, mats[0].shape[0], 1)
    removed = []

    paths = []
    genome_paths = []
    costs = []

    all_removed = False

    seeds = []

    for i in range(n_seeds):

        sp_thresh = 1000
        gen_thresh = 10

        seed = next_seed(
            mats,
            all_dots,
            removed,
            sp_thresh,
            gen_thresh,
            n_tries=1
        )

        seeds.append(seed)

        if seed is None:
            # there were no valid seeds to start from
            break


        # make copy of mats just for this seed
        # we will delete duplicate alleles at each step
        mats_local = [ m.copy() for m in mats ]

        path = [seed]
        genome_path = [mats_local[2][seed]]
        cost = [0]

        curpos = seed
        curloc = genome_path[-1]


        i = 0

        while i < 1000:

            i+=1

            if sp_thresh > sp_thresh_max:
                break

            if gen_thresh > gen_thresh_max:
                break


            nextmoves = get_valid_connections(
                mats_local, 
                curpos,
                allow_back=False,
                spatial_thresh=sp_thresh,
                genomic_thresh=gen_thresh,
                path=path,
                master=mats_master,
                path_max_dist=1500
            )

            if len(nextmoves[0]) == 0:
                sp_thresh += 100
                gen_thresh += 1
                continue

            nextpos = nextmoves[0][0]
            nextcost = nextmoves[1][0]
            nextloc = mats_local[2][nextpos]

            path.append(nextpos)
            path = sorted(path)
            cost.append(nextcost)
            genome_path.append(nextloc)
            genome_path = sorted(genome_path)

            removed.append(curpos)
            # remove the current node from the matrix
            mats_local = remove_node(mats_local, curpos)
            # remove any other copies of this locus as well
            mats_local = remove_locus(mats_local, node=curpos)

            curpos = nextpos
            curloc = nextloc

            #if len(removed) > 0.9*mats[0].shape[0]:
            #    all_removed = True
            #    break

            if len(path) > 0.4*mats[0].shape[0]:
                path_full = True
                break


        paths.append(path)
        costs.append(cost)
        genome_paths.append(genome_path)

        # remove all nodes from that path 
        mats = remove_node(mats, path)
        removed += path

        if all_removed:
            break

    return paths, genome_paths, costs

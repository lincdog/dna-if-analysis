import numpy as np
import pandas as pd
import scipy
import scipy.spatial as sp
import os
import os.path as osp
import matplotlib.pyplot as plt
import tables
import re

"""
distance.py: classes to convert CSV tables of data to compressed H5 format
also contains classes for computing contact maps and cell ensemble computations
main focus is to keep RAM usage down
"""


class DistExperiment:
   

    def __init__(
        self,
        raw=None,
        order=None,
        coords=('x', 'y', 'z'),
        pixel_size_nm=(103., 103., 250.),
        order_column='Region ID',
        raw_merge_column=None,
        order_merge_column=None,
        chrom_column='Chrom ID',
        cell_columns=['fov', 'cellID'],
        exp_name=None,
        dtype=np.uint16,
        max_ram=5e9,
        h5_filename=None,
        convert_units=True
    ):
        
        if raw is not None and order is not None:
            
            converters = { 
                coord: px
                    for coord, px in zip(coords, pixel_size_nm)
            }

            if type(raw) is str:
                
                try:
                    raw_df = pd.read_csv(
                        raw
                    )
                except FileNotFoundError:
                    raise FileNotFoundError(f'No such file for raw df {raw}')

            elif type(raw) is pd.DataFrame:
                raw_df = raw
            else:
                raise TypeError('Must either provide a string filename or a DataFrame')

            if convert_units:
                for col, coeff in converters.items():
                    raw_df[col] = (coeff * raw_df[col])

            if type(order) is str:
                try:
                    order_df = pd.read_csv(
                       order,
                    )
                except FileNotFoundError:
                    raise FileNotFoundError(f'No such file for order df {order}')

            elif type(order) is pd.DataFrame:
                order_df = order
            else:
                raise TypeError('Must either provide a string filename or a DataFrame')


            if raw_merge_column is None:
                raw_merge_column = order_column

            if order_merge_column is None:
                order_merge_column = order_column

            assert all(np.isin(cell_columns, raw_df.columns))
            assert raw_merge_column in raw_df.columns

           
            assert order_merge_column in order_df.columns

            merged_raw = pd.merge(
                raw_df, 
                order_df, 
                left_on=raw_merge_column,
                right_on=order_merge_column,
                how='inner',
            )
        
            # Set instance variables

            self.merged_raw = merged_raw.copy()
            self.order_df  = order_df.copy()
            
            # Dataframe of unique cells
            self.cell_df = pd.DataFrame.from_records(
                list(merged_raw.groupby(cell_columns).groups.keys()),
                columns=cell_columns
            )
            
            self.info = dict(
                # Name of ordering column that imposes order on the data
                order_column=order_column,
                # Length of coords_df is number of possible values of coords
                order_len=len(order_df),
                # Pixel size used for converting distance
                pixel_size_nm=pixel_size_nm,
                distance_units='nm',
                # Coordinate column names
                coords=coords,
                # Columns in raw_df used to uniquely identify each cell. 
                cell_columns=cell_columns,
                n_cell_columns=len(cell_columns),
                # Number of unique cells
                n_cells=len(self.cell_df),
                # Data type of distance matrices
                dtype=dtype,
                # Identifying name for this experiment
                exp_name=exp_name,
                # Conservative maximum estimate of RAM usage in bytes for an in-memory data structure
                max_ram=max_ram,
                # Flag for whether pdists were computed
                has_pdists=False,
            )

            del raw_df, merged_raw, order_df
            
        elif h5_filename is not None:
            # No raw or merged df; only order df
            h5_file = pd.HDFStore(h5_filename, 'r+')

            self.cell_df = h5_file.get(key='cell_df')
            self.order_df = h5_file.get(key='order_df')
            
            self.info = h5_file.root.info._v_attrs.info
            
            self.h5_file = h5_file
            
            self.merged_raw = None
            
            
            
    
    def compute_pdists(
        self,
        h5_filename=None,
        h5_title=None,
        sort_columns=None,
        which=None,
        in_memory=False,
        overwrite=False,
        print_update=None
    ):
        if sort_columns is None:
            sort_columns = [*self.info['cell_columns']] + [self.info['order_column']]
            
        if h5_title is None:
            h5_title = self.info['exp_name']
        
        if h5_filename is None:
            h5_filename = ''.join(h5_title, '.h5')
            
        # FIXME: This is not quite working for individual tuples for now. 
        # Don't think it's a major use case though.
        if type(which) is list or type(which) is tuple:
            query_string = f''
            operating_df = self.merged_raw
        
        operating_df = self.merged_raw.sort_values(by=sort_columns)
        
        operating_df_grp = operating_df.groupby(self.info['cell_columns'])
        
        
        filters = tables.Filters(complevel=9)
        
        if osp.exists(h5_filename) or self.info['has_pdists']:
            if overwrite:
                os.remove(h5_filename)
            else:
                raise OSError(
                    f'File {h5_filename} exists already or pdists already computed, but overwrite is False.')
                
                
        h5_file = tables.open_file(h5_filename, mode='a', title=h5_title, filters=filters)

        
        self.order_df.to_hdf(h5_filename, mode='r+', key=f'order_df', append=True)
        self.cell_df.to_hdf(h5_filename, mode='r+', key=f'cell_df', append=True)
        
        self.info['has_pdists'] = True
        info_group = h5_file.create_group('/', 'info')
        info_group._v_attrs.info = self.info
        
        h5_file.flush()

        
        if in_memory:
            locus_orders = []
            dist_vecs = []
        
        cells_group = h5_file.create_group('/', 'cells')


        i_cell = 0
        n_cells = self.info['n_cells']

        for info, group in operating_df_grp:
          
            group_name = self.info2group(dict(zip(self.info['cell_columns'], info)))

            if print_update:
                
                try:
                    rem = int(i_cell % print_update)

                    if rem == 0:
                        print(f'Beginning cell group `{group_name}` ({i_cell}/{n_cells})')
                except TypeError:
                    pass

            i_cell += 1
            
            cell = h5_file.create_group('/cells/', group_name)
            
            # Set metadata as attrs
            cell._v_attrs.info = { col: val 
                for col, val in zip(self.info['cell_columns'], info) }
            
            # Order of loci to recover distance matrix indices
            locus_order = group[self.info['order_column']].values
            
            # Compute pairwise distances from ndarray of self.coords columns and cast to appropriate type
            dist_vec = scipy.spatial.distance.pdist(
                group[[*self.info['coords']]].values).astype(self.info['dtype'])

            

            order_carray = h5_file.create_carray(cell, 
                                                 'locus_order', 
                                                 tables.UInt32Atom(), 
                                                 shape=(len(locus_order),), 
                                                 filters=filters)

            pdist_carray = h5_file.create_carray(cell, 
                                                 'pdists', 
                                                 tables.Atom.from_dtype(
                                                    np.dtype(self.info['dtype'])), 
                                                 shape=(len(dist_vec),), 
                                                 filters=filters)


            order_carray[:] = locus_order
            pdist_carray[:] = dist_vec

            h5_file.flush()
            
            if in_memory:
                dist_vecs.append(dist_vec)
                locus_orders.append(locus_orders)
            else:
                del dist_vec, locus_order
        
    
        self.h5_file = h5_file
        
        if in_memory:
            return (dist_vecs, locus_orders)
        
        
        return
    
    @classmethod
    def from_hdf(
        cls,
        filename,
    ):
        
        return cls(h5_filename=filename)
        
        
    
    
    def ensemble_compute(
        self,
        func=None,
        dups_func=np.mean,
        which=None,
        order_column=None,
        in_memory=False,
        dtype=np.uint16,
        dups_dtype=np.uint16,
        **kwargs,
    ):

    # Applies computations between cell distance matrices, i.e.
    # computations requiring either accumulating results from each cell's matrix,
    # or actually keeping each cell's matrix in memory simultaneously to perform
    # aggregation (e.g., medians across all cells for each locus pair)
    # `func` should expect 2 arguments, the accumulator and the current matrix
    # if all are kept in memory, the accumulator will be the stack so far.
        
        if in_memory:
            stack = []
        else:
            assert func is not None

            accum = np.zeros(
                (self.info['order_len'], self.info['order_len']),
                dtype=dtype
            )

        if order_column is None:
            order_column = self.info['order_column']

        order_column = list(np.ravel([order_column]))


        for cell in self.h5_file.root.cells:
            mat = self.get_matrix(cell._v_name)
            mat.construct_index(order_column=order_column, sort=True)
            mat.collapse_dups(dups_func, dups_dtype)
            mat.reindex_coords()

            if in_memory:
                stack.append(mat.values)
            else:
                accum = func(accum, mat, **kwargs)







    def apply(
        self,
        func,
        title=None,
        dups_func=None,
        dtype=None,
        dups_dtype=None,
        **kwargs
    ):

    # Applies a function within one distance matrix - for example arithmetic
    # or conditionals on its entries.  `func` should excpect at least one argument,
    # a DataFrame distance matrix.

        if dtype is None:
            dtype = self.info['dtype']

        if dups_dtype is None:
            dups_dtype = self.info['dtype']




    
    def get_matrix(
        self,
        cell,
    ):

        if self.h5_file is None:
            raise ValueError('To call fetch_matrix, an h5_file must be open.')

        if type(cell) is pd.DataFrame or type(cell) is dict:
            groupname = self.info2group(cell)
        elif type(cell) is str:
            groupname = cell
        else:
            raise TypeError('`get_matrix` must be supplied with a DataFrame, dict, or string')

        cell_group = self.h5_file.get_node(f'/cells/{groupname}')

        locus_order = cell_group.locus_order.read()
        dist_vec = cell_group.pdists.read()


        return DistMatrix(
            dist_vec,
            groupname,
            cell_group._v_attrs.info,
            parent=self,
            index=locus_order,
            order_df=self.order_df,
            order_column=self.info['order_column'],
            dtype=self.info['dtype'],
        )
    
    @staticmethod
    def info2group(info):
        # Take an info row or DF and convert to canonical group name
        if type(info) is pd.DataFrame:
            keys = info.columns.values
            vals = info.values
            
            return '_'.join([f'{k}{v}' for k, v in zip(keys, vals)])
        elif type(info) is dict:
            return '_'.join([f'{k}{v}' for k, v in info.items()])
        
        
    @staticmethod   
    def group2info(group):
        # Take a groupname and convert to dict
        parts = group.split('_')
        
        matches = [ re.search('')]
    

class DistMatrix:
   

    def __init__(
        self,
        data,
        groupname,
        info,
        parent=None,
        units='nm',
        index=None,
        order_df=None,
        order_column=None,
        dtype=np.uint16,
        attrs=None,
    ):
    
        if type(data) is pd.DataFrame:
            self.m = data.copy()
            del data
        else:
           
            data = np.array(data, dtype=dtype)
            if len(data.shape) == 1:
                self.data = sp.distance.squareform(data).astype(dtype)
            elif len(data.shape) == 2:
                self.data = data.astype(dtype)
            else:
                raise np.AxisError('Dimension of distance data must be 1 or 2')

            self.m = pd.DataFrame(
                self.data, 
                index=index, 
                columns=index,
                dtype=dtype,
            )
        
        self.parent = parent
        self.groupname = groupname
        
        self.info = info
        self.units = units
        self.order_df = order_df
        self.order_column = order_column
        self.dtype = dtype
        self.has_index = False
        self.collapsed = False
        self.has_nans = False
        self.aligned = False
    
                            
    @classmethod
    def from_hdf(
        cls,
        filename,
        
    ):
        pass
    
    
    def to_hdf(
        self,
        filename,
        groupname=None,
        squareform=False,
        overwrite=False,        
    ):

        if osp.exists(filename):
            if overwrite:
                os.remove(filename)
            else:
                raise OSError(f'File {filename} exists but overwrite is not set.')

        h5_file = tables.open_file(filename, 'a')

        if 'cells' not in h5_file.root._v_groups.keys():
            cells_group = h5_file.create_group('/', 'cells')

        if groupname is None:
            groupname = self.groupname

        new_group = h5_file.create_group('/cells/', groupname)
        new_group._v_attrs.info = self.info

        h5_file.flush()

        self.m.to_hdf(filename, key=groupname, complevel=9)

        h5_file.close()

    
    def construct_index(
        self,
        order_df=None,
        order_columns=None,
        sort=False,
    ):
        if order_df is None:
            order_df = self.order_df


        order_columns = list(np.ravel([order_columns]))

        assert self.order_column in order_columns
        assert self.order_column in order_df.columns
        assert self.has_index == False

        new_index = pd.MultiIndex.from_frame(
            pd.merge(order_df, 
                    self.m.index.to_frame(name=self.order_column),
                    on=self.order_column, 
                    how='right'
                    )[order_columns]
            )
        self._order_column = self.order_column
        self.order_column = order_columns
        self.m.index = new_index
        self.m.columns = new_index

        if sort:
            self.m.sort_index(axis=0, level=0, sort_remaining=False, inplace=True)
            self.m.sort_index(axis=1, level=0, sort_remaining=False, inplace=True)

        self.has_index = True

    def to_ma(
        self,
        mask_value=None,
        fill_value=2**16-1,

    ):


        if mask_value is None:
            mask = self.m.isna().values
        else:
            mask = self.m == mask_value

        return np.ma.array(
            self.m.values, 
            mask=mask, 
            fill_value=fill_value, 
            dtype=self.dtype
        )

    
    def collapse_dups(
        self,
        agg=None,
        dtype=None
    ):
        if dtype is None:
            dtype = self.dtype

        self._m = self.m.copy()
        self.m = self._m.groupby(by=self.order_column, axis=0).agg(agg).astype(dtype)
        self.m = self.m.groupby(by=self.order_column, axis=1).agg(agg).astype(dtype)

        self.collapsed = True
    
    def reindex_coords(
        self,
        order_df=None,
        order_columns=None,
        fill_value=None
    ):
        if order_df is None:
            order_df = self.order_df

        if order_columns is None:
            order_columns = np.ravel([self.order_column])


        full_order_index = pd.MultiIndex.from_frame(order_df[order_columns])

        self._m = self.m.copy()

        self.m = self._m.reindex(full_order_index, fill_value=fill_value, axis=0)
        self.m = self.m.reindex(full_order_index, fill_value=fill_value, axis=1)

        if self.m.isna().any().any():
            self.has_nans = True

        self.aligned = True
    
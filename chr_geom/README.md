# Chromosome geometry and density

This folder contains code to work with chromosome territory geometry and create density estimates
for chromosomes and IF markers.

## Chromosome territories

A difficulty is that we don't *a priori* have any global coordinate system that is relevant.
Thus, we can only really work with relative distances. But we can potentially define a radial
distance, or we can define mappings between the chromosome configurations of different cells,
and the mapping tells you how similar/different they are. Interpreting the density of each chromsoome
as a probability distribution possibly allows one to use probability distribution
similarity measures, though if they are dependent on the coordinatization of the support it would 
not work. 

Doing some sort of shape matching in the 20D space to 'align' cells and analyze
their similarity seems interesting, even just for the individual loci in 3D.
Gromov-Hausdorff distance metric does fulfill the idea of rotation-invariant similarity
Subtracting/comparing single cell chromosome proximity matrices also gets at this in some way
that is absolute position/angle invariant, but discards this information
It's possible that more literally modeling 'moving' each chromosome from its position
in cell A to its position in cell B is more general and could be interesting.

But for just considering territories, it should be sufficient to estimate density across
all voxels to unify with the IF data, and look at how density of different chromosomes
overlaps. Then we can also ask about the "local environment" in terms of chromosome 
and IF density for specific loci. We can construct various surfaces using (super)level sets of 
the density.




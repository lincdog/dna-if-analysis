# To-do list for DNA/IF analysis

## Technical development

### Allele separation

On 1 Mb data - can use agglomerative clustering with connectivity matrix???

roughly, maximize genomic coverage in each cluster while minimizing some measure of spread (sillhouette...) 
possibly actually traverse points in chromosome order, assigning to each allele as you go; if already found locus X once, know subsequent appearances are different allele.
This is sort of like a k means (or dbscan?) thing where first few loci define "seed" cluster, subsequent ones possibly assigned to closest seed; maybe minimize total path distance traversed in each allele? because we sort of know that chromosomes don't tend to have extremely large excursions of only a few loci.
need to visualize results conveniently and fast to evaluate methods - can do this in paraview coloring by allele, but is it fast enough? can i load many cells at once and flip thru them?

### Unified file format

Combined metadata and spatial data, hierarchical and arbitrary complexity. Probably use pytables. Ideally, integrate with 3D visualization of arbitrary meshes through paraview or something else

### 3D visualization/data exploration

* Paraview is working fairly well
    * Nice builtin GUI that allows some important operations like contouring (isosurfaces), thresholding, associating and coloring by various scalars (genomic coord, chromosome, allele, ...)
* There are also options that work directly in Jupyter: pyvista has fairly advanced/featureful interactive plotting, and giotto-tda uses plotly library (can do 3d but not sure about rendering meshes) - also produces Dash to make more interactive apps in python
    * Panel from Anaconda - higher level functions than bokeh (maybe no JS?), I think this is what Justin has the new bebi103 students use

## Biological insight

### Loop detection

* TDA related - detection of loops/shells and seeing what is in those shells and on the surface
    * Witness complex may be more interpretable for specific loci of interest
* Graph-based - detect cycles in induced graph from proximity matrix (this is basically just homology)

### Surface lying detection

* Possibly - 3D intensity (height) filtration on image data; possibly with density estimate of chromosomes and mapper; as isosurface is expanded should see rapid increase in density at some surface.


### Long-range interaction detection

* Also graph-based using edge weights (distances)
* Image-based using distance matrices themselves
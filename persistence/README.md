# Persistence

This folder contains code and notebooks to analyze data using persistent homology 
by constructing various filtered simplicial complexes from the data.

There are several types of complexes one can generate using available algorithms in GUDHI,
scikit-TDA, or others:
* Vietoris-Rips complex
* Alpha complex
* (Euclidean) Witness complex (requires two not necessarily disjoint sets of points "witness" and "landmark" set - maybe useful for DNA-IF interaction, or fixed loci)
* Cover complexes (what mapper creates): you get a cover of your point cloud (collection of sets whose unions is the whole set), often from a low-D function of the values, then construct simplicial complex using nerve, which places a k-simplex wherever k cover elements intersect.
* Tangential complex: purpose is to reconstruct a k-dimensional manifold in d-dimensional Euclidean space. Linear on d, exponential on k. Possibly good for chromatin-IF high dimensional data, to find lower dimensional features
* 

Note that GUDHI has a few more algorithms available in the C++ library, and the C++ library CGAL has many, many
more computational geometry tools, many of them focusing on 2 or 3D Euclidean space though.

The general idea is to produce some sort of tree based on smoothly varying some parameter.
Roughly, the parameter controls the "scale" at which you consider features. At each
parameter value, you see which points have overlapping or nonoverlapping features. So you
build up a "birth/death" timeline of various features which could be connected components,
loops, 2D holes, or higher dimensional holes. The kind of filtration you construct determines
what kind of features you detect. The most basic and standard ones are mostly some variation on
opening a ball around each point and recording when these balls overlap. Formally, a filtration must
be monotonically increasing with respect to inclusion of points in the argument set. This allows
a filtration to be an ordering for a simplicial complex.

## Principles and goals

It's hard for me to justify using any particular complex over the others and it may be hard
to interpret the results from them if I don't have specific principles guiding my choices.
At the least principled level, I am thinking of just apply a few of these basic
filtrations to the point cloud for each cell and generating persistence diagrams,
then using metrics like bottleneck or wasserstein distance just to see what the general
shape of the data is. But I think the only possibility for real learning from this would be
if there are some cells with markedly different diagrams than others, and if we can check those cells
for other features in the further dimensions of the data. We can expect that different nuclear size,
cell cycle stage would result in pretty obvious changes to the 3D shape and topology of the point cloud
as a whole. I need to figure out how to go from interesting features
revealed in a persistence diagram to the original points/loci that compose those features.
For example, let's say there is H1 or H2 significant persistence. What loci make up that 1- or 2-D loop?


What I have is a list of ~2-5k 3D points over ~400 cells (1 Mb resolution data) along with their genomic
information. There are 20 chromosomes in each. In addition, on the same coordinate system I have voxel values for ~20 IF
markers. Relating interesting persistent features to what the IF looks like in the region would be important.